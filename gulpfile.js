// Gulp
var gulp = require('gulp');

// Plugins
var twig = require('gulp-twig');

var runSequence = require('run-sequence');

var rimraf = require('gulp-rimraf');
var plumber = require('gulp-plumber');
var browserSync = require("browser-sync");

// Wipe off build folder
gulp.task('clean', function() {
  return gulp.src(['Build', 'public'], {
      read: false
    })
    .pipe(rimraf());
});

// Twig to HTML
gulp.task('twig', function() {
  gulp.src(['mockups/*.twig', '!./{node_modules/**, node_modules}'])
    .pipe(plumber())
    .pipe(twig({
      pretty: true
    }))
    .pipe(gulp.dest('public/'))
    .pipe(browserSync.reload({
      stream: true
    }));
});
// Copy
gulp.task('copy', function() {
    gulp.src(['assets/**'])
        .pipe(gulp.dest('public'))
});

// Livereload
gulp.task('listen', function() {
  browserSync({
    server: {
      baseDir: "./public/"
    },
  });
});

gulp.task('wait', function(end) {
  //wait(5000);
  end();
});

gulp.task('reload', function() {
  browserSync.reload();
});

// Watch files
gulp.task('watch', function(event) {
  gulp.watch(['mockups/*.twig', 'layouts/*.twig'],
    ['twig', browserSync.reload]);
});

gulp.task('build', function(callback) {
  runSequence('wait',
      'clean',
      'copy',
      'twig');
});

gulp.task('default', function(callback) {
  runSequence('wait', 'clean', 'copy',
      'twig',
    'listen',
    'watch');
});
